# -*- coding: utf-8 -*-
import datetime
from slammer import db

FIELD_CHOICES = [u'Cпокойный', u'Нервозный', u'Злой', u'Инфернальный', u'Сотона']

class Prisoner(db.DynamicDocument):
    first_name = db.StringField(max_length=255, required=True, verbose_name = 'First name')
    last_name = db.StringField(max_length=255, required=True, verbose_name = 'Last name')
    second_name = db.StringField(max_length=255, required=True, verbose_name = 'Second name')
    dead = db.BooleanField(required=False, verbose_name = 'Dead') # I think BooleanField more logically
    aggression_level = db.StringField(choices=FIELD_CHOICES, required=True, verbose_name = 'Aggression level')

    release_date = db.DateTimeField(default=datetime.datetime.now, required=True, verbose_name = 'Release date')
    release_date_real = db.DateTimeField(default=datetime.datetime.now, required=True,
                                         verbose_name = 'Real release date')
    arrived = db.DateTimeField(required=True, verbose_name = 'Arrived date')

    def get_years_count(self):
        self.years_count = self.arrived-self.release_date
        return str(float(self.years_count.days)/365) or '-'

    def __unicode__(self):
        return self.last_name

    meta = {'indexes': ['aggression_level'],
            'ordering': ['last_name']}


'''
Future implementation

class TermsOfCrimes(db.ReferenceField):
    release_date = db.DateTimeField(default=datetime.datetime.now, required=True, verbose_name = 'Release date')
    release_date_real = db.DateTimeField(default=datetime.datetime.now, required=True, verbose_name = 'Real release date')
    arrived = db.DateTimeField(default=datetime.datetime.now, required=True, verbose_name = 'Arrived')

    meta = {
        'indexes': ['-release_date'],
        'ordering': ['-release_date']
    }
    def get_years_count(self):
        years_count = self.arrived-self.release_date
        return str(float(years_count.days)/365) or '-'
'''