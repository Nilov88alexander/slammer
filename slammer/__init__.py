from flask import Flask
from flask.ext.mongoengine import MongoEngine
from flask.ext.admin import Admin

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {'DB': "slammer"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"
db = MongoEngine(app)
'''
Future implementation

def register_blueprints(app):
    # Prevents circular imports
    from slammer.views import prisoners_app
    from slammer.my_admin import my_admin
    app.register_blueprint(prisoners_app)
    app.register_blueprint(my_admin)
register_blueprints(app)
'''
# Add Admin interface:
custom_admin = Admin(app)
from models import Prisoner
from admin import PrisonerModelView
custom_admin.add_view(PrisonerModelView(Prisoner))


if __name__ == '__main__':
    app.run()