from flask.ext.admin.contrib.mongoengine import ModelView

class PrisonerModelView(ModelView):
    column_searchable_list = ['first_name', 'last_name','second_name',]
    column_filters = ['aggression_level','dead']
    page_size = 20
    column_editable_list = ['dead']